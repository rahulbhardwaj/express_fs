const path = require("path");
const fs = require("fs").promises;

const requestListener = async (req, res) => {
  const bodyObject = req.body;
  const storagePath = path.join(__dirname, "..", "public", "directories");

  if (Object.keys(bodyObject).length === 0) {
    const storageStructure = await readStorageStructure(storagePath);
    const message = {
      welcome: "Welcome to Express FS Drill",
      "To Create Files":
        "send post request with data as row json as given format",
      "To Delete Files":
        "send delete request with data as row json as given format",
      Format: {
        directoryName: "directory1",
        fileNames: ["file1.json", "file2.json"],
      },
      "Current Storage Structure": storageStructure,
    };
    res.setHeader("Content-Type", "application/json").send(message);
  } else if (req.method === "POST") {
    const directoryName = bodyObject.directoryName;
    const fileNames = bodyObject.fileNames;
    const result = await createFiles(directoryName, fileNames);
    const storageStructure = await readStorageStructure(storagePath);
    if (result.isSuccess) {
      const message = {
        result: result.message,
        "Current Storage Structure": storageStructure,
      };
      res.setHeader("Content-Type", "application/json").send(message);
    } else {
      const message = {
        result: "Error while creating files",
        ErrorDetails: result.message,
        "Current Storage Structure": storageStructure,
      };
      res
        .status(500)
        .setHeader("Content-Type", "application/json")
        .send(message);
    }
  } else if (req.method === "DELETE") {
    const directoryName = bodyObject.directoryName;
    const fileNames = bodyObject.fileNames;
    const result = await deleteFiles(directoryName, fileNames);
    const storageStructure = await readStorageStructure(storagePath);
    if (result.isSuccess) {
      const message = {
        result: result.message,
        "Current Storage Structure": storageStructure,
      };
      res.setHeader("Content-Type", "application/json").send(message);
    } else {
      const message = {
        result: "Error while Deleting files",
        "Error Details": result.message,
        "Current Storage Structure": storageStructure,
      };
      res
        .status(500)
        .setHeader("Content-Type", "application/json")
        .send(message);
    }
  } else {
    const message = { Error: "Wrong Request" };
    res.status(400).setHeader("Content-Type", "application/json").send(message);
  }
};

module.exports = requestListener;

async function createFiles(directoryName, fileNames) {
  const directoryPath = path.join(
    __dirname,
    "..",
    "public",
    "directories",
    directoryName
  );
  try {
    await fs.access(directoryPath);
  } catch (err) {
    console.log(err);
    try {
      await fs.mkdir(directoryPath);
    } catch (err) {
      console.log(err);
      return { isSuccess: false, message: "Error while creating Directory" };
    }
  } finally {
    let createdFiles = [];
    let notCreatedFiles = [];
    for (let index = 0; index < fileNames.length; index++) {
      const randomData = JSON.stringify(Math.random());
      const filePath = path.join(directoryPath, fileNames[index]);
      try {
        await fs.writeFile(filePath, randomData);
        createdFiles.push(fileNames[index]);
      } catch (err) {
        console.log(err);
        notCreatedFiles.push(fileNames[index]);
      }
    }
    if (JSON.stringify(notCreatedFiles) === "[]") {
      return { isSuccess: true, message: "Success, All files Created" };
    } else {
      if (JSON.stringify(createdFiles) === "[]") {
        createdFiles = "no files";
      }
      return {
        isSucess: false,
        message: `Created ${createdFiles} , Error while creating file(s) ${notCreatedFiles}`,
      };
    }
  }
}

async function deleteFiles(directoryName, fileNames) {
  const directoryPath = path.join(
    __dirname,
    "..",
    "public",
    "directories",
    directoryName
  );
  try {
    await fs.access(directoryPath);
  } catch (err) {
    console.log(err);
    return { isSuccess: false, message: "Directory Does not exists" };
  }
  let deletedFiles = [];
  let notDeletedFiles = [];
  for (let index = 0; index < fileNames.length; index++) {
    const filePath = path.join(directoryPath, fileNames[index]);
    try {
      await fs.unlink(filePath);
      deletedFiles.push(fileNames[index]);
    } catch (err) {
      console.log(err);
      notDeletedFiles.push(fileNames[index]);
    }
  }
  if (JSON.stringify(notDeletedFiles) === "[]") {
    return { isSuccess: true, message: "Deleted all mentioned files" };
  } else {
    if (JSON.stringify(deletedFiles) === "[]") {
      deletedFiles = "no files";
    }
    return {
      isSuccess: false,
      message: `Deleted ${
        deletedFiles || "no files"
      } ,Error while deleting file(s) ${notDeletedFiles}`,
    };
  }
}

async function readStorageStructure(storagePath) {
  try {
    const storageStructure = {};
    const directoryNames = await fs.readdir(storagePath);
    for (index = 0; index < directoryNames.length; index++) {
      const directoryNames = await fs.readdir(storagePath);
      const subDirectoryPath = path.join(storagePath, directoryNames[index]);
      const fileNames = await fs.readdir(subDirectoryPath);
      storageStructure[directoryNames[index]] = { ...fileNames };
    }
    return storageStructure;
  } catch (err) {
    console.log(err);
    return { Error: "Error while reading Directory" };
  }
}
